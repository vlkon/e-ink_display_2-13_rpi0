# e-ink display for RPi0
2.13 e-ink display for Raspberry Pi 0

features:
* vertical orientation
* time
    * hour:minute
* date
    * day/month
* temperature in/out
    * 2 sensors DS18B20 connected on 1-wire interface (GPIO connection)
* temperature forecast
    * data from yr.no based on GPS coordinates
    * prediction for next 3 hours (easy to extend but not much room on the display where to print)

### Prerequisites
Only tested on:
Raspberry Pi Zero (wh) with Raspbian GNU/Linux 10 (buster)
Waveshare e-ink display 2.13" (v2)

important libraries:
bcm2835 - not part of standard apt packages
curl (7.64.0)

### how to
build:
`make`

remove build:
`make clean`

install - copy executable to /opt structure and enable systemd service at startup
`make install`

uninstall - stops systemd service and remove exacutable
`make uninstall`

(un)install_reset_timer - systemd timer that resets the service once per day. Somehow the display freezes after few days of running and I have to restart it.
`make install_reset_timer`
`make uninstall_reset_timer`

### example
![epd](example/epd.png)
