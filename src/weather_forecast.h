
#ifndef __WEATHER_FORECAST_H_
#define __WEATHER_FORECAST_H_

#include "weather_config.h"
#include "error.h"

//HTTP return codes
#define HTTP_OK         200

#define FORECAST_SIZE   3

typedef struct{
    int valid;      // error code that is also returned from forecast function
    double temperature[FORECAST_SIZE];
    double precipitation[FORECAST_SIZE];
}weather_struct;

//int forecast(weather_struct* overview);
ERROR_T get_forecast(weather_struct* forecast);
ERROR_T download_forecast(void);
ERROR_T is_expired(const char* path, int random_delay);

#endif  // __WEATHER_FORECAST_H_