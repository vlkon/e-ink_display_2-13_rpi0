
#include "main.h"
#include <signal.h>

volatile sig_atomic_t signal_flag;


void  sig_int(int signal)
{
    signal_flag = signal;
}


void  sig_term(int signal)
{
    signal_flag = signal;
}


void build_gui()
{
    Paint_DrawRectangle(1, 1, EPD_2IN13_V2_WIDTH, EPD_2IN13_V2_HEIGHT, BLACK, DOT_PIXEL_2X2, DRAW_FILL_EMPTY);     // frame edge

    Paint_DrawLine(0, 64, EPD_2IN13_V2_WIDTH, 64, BLACK, DOT_PIXEL_2X2, LINE_STYLE_SOLID);         // horizontal clock-date separation

    // forecast frame
    Paint_DrawLine(0, 121, EPD_2IN13_V2_WIDTH, 121, BLACK, DOT_PIXEL_2X2, LINE_STYLE_SOLID);         // horizontal date-forecast separation
    Paint_DrawLine(0, 124, EPD_2IN13_V2_WIDTH, 124, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);         // horizontal date-forecast separation
    Paint_DrawLine(0, 191, EPD_2IN13_V2_WIDTH, 191, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);         // horizontal forecast-temperature separation
    Paint_DrawLine(0, 193, EPD_2IN13_V2_WIDTH, 193, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);         // horizontal forecast-temperature separation
    Paint_DrawLine(73, 124, 77, 191, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);       // vertical forecast temperature-precipitation separator
    Paint_DrawLine(75, 124, 79, 191, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);       // vertical forecast temperature-precipitation separator

    //temperature - inside/outside marking
    Paint_DrawLine(10, 222, 10, 229, BLACK, DOT_PIXEL_2X2, LINE_STYLE_SOLID);       // 'I'
    Paint_DrawCircle(22, 240, 4, BLACK, DOT_PIXEL_2X2, DRAW_FILL_EMPTY);            // 'O'
    Paint_DrawLine(20, 215, 10, 250, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);       // separator line
    Paint_DrawLine(0, 215, 20, 215, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);        // marking frame
    Paint_DrawLine(20, 215, 30, 225, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);       // marking frame
    Paint_DrawLine(30, 225, 30, 250, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);       // marking frame
}


UPDATE_T get_time(PAINT_TIME* pTime)
{
    //get current system time
    time_t rawtime;
    struct tm* timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    pTime->year = (UBYTE)(1900 + timeinfo->tm_year);
    pTime->month = (UBYTE)(1 + timeinfo->tm_mon);
    pTime->day = (UBYTE)timeinfo->tm_mday;

    pTime->sec = (UBYTE)timeinfo->tm_sec;
    if((pTime->hour != (UBYTE)timeinfo->tm_hour) || (pTime->min != (UBYTE)timeinfo->tm_min)){
        pTime->hour = (UBYTE)timeinfo->tm_hour;
        pTime->min = (UBYTE)timeinfo->tm_min;
        if(pTime->min % REFRESH_RATE == 0){return UPDATE_FULL;}
        return UPDATE_PART;
    }else{
        return UPDATE_NONE;
    }
}


void update_time(const PAINT_TIME* time)
{
    #define TIME_FONT    font34x16
    const int x_origin = 21;
    const int y_origin = 15;
    const int str_size = 5;

    Paint_ClearWindows(x_origin, y_origin, x_origin + TIME_FONT.width*str_size, y_origin + TIME_FONT.height, WHITE);
    Paint_DrawTimeWide(x_origin, y_origin, time, &TIME_FONT, BLACK, WHITE);
    #undef TIME_FONT
}


void update_date(const PAINT_TIME* time)
{
    #define DATE_FONT    font30x16
    const int x_origin = 21;
    const int y_origin = 79;
    const int str_size = 5;

    Paint_ClearWindows(x_origin, y_origin, x_origin + DATE_FONT.width*str_size, y_origin + DATE_FONT.height, WHITE);
    Paint_DrawDateWide(x_origin, y_origin, time, &DATE_FONT, BLACK, WHITE);
    #undef DATE_FONT
}


inline int max(int a, int b)
{
    return a > b ? a : b;
}


ERROR_T update_forecast(const weather_struct* forecast)
{
    #define FORECAST_FONT    font15x9
    const int x_origin_temp = 5;
    const int y_origin_temp = 130;
    const int x_origin_prec = 79;
    const int y_origin_prec = y_origin_temp;

    const int y_pixel_gap = 5;
    const int x_pixel_gap = 2;

    const int temp_str_size = 7;    // big enough to hold temperature format "% 05.1f"
    const int prec_str_size = 4;    // big enough to hold precipitation format "%04.1f"

    char buff[max(temp_str_size, prec_str_size)];  // big enough buffer to store forecast entry as a string

    for(int i = 0; i<FORECAST_SIZE; i++){
        // temperature
        Paint_ClearWindows(x_origin_temp + x_pixel_gap * i                                        ,
                           y_origin_temp + y_pixel_gap * i + FORECAST_FONT.height * i             ,
                           x_origin_temp + x_pixel_gap * i + FORECAST_FONT.width  * temp_str_size ,
                           y_origin_temp + y_pixel_gap * i + FORECAST_FONT.height * (i + 1)       ,
                           WHITE);
        if(forecast->valid == ERR_OK){
            sprintf(buff, "% 05.1f", forecast->temperature[i]); // turn temperature into a string value
            Paint_DrawTemperatureWide(x_origin_temp +                         x_pixel_gap  * i,
                                      y_origin_temp + (FORECAST_FONT.height + y_pixel_gap) * i,
                                      buff, &FORECAST_FONT, BLACK, WHITE);
        }

        // precipitation
        Paint_ClearWindows(x_origin_prec + x_pixel_gap * i                                          ,
                           y_origin_prec + y_pixel_gap * i + FORECAST_FONT.height * i               ,
                           x_origin_prec + x_pixel_gap * i + FORECAST_FONT.width  * prec_str_size   ,
                           y_origin_prec + y_pixel_gap * i + FORECAST_FONT.height * (i + 1)         ,
                           WHITE);
        if(forecast->valid == ERR_OK){
            double tmp_double = 0.0;
            if(forecast->precipitation[i] > 99.9)
            {
                tmp_double = 99.9;  //can't be negative but upper limit is unknown
            }else{
                tmp_double = forecast->precipitation[i];
            }
            sprintf(buff, "%04.1f", tmp_double); // turn precipitation into a string value

            Paint_DrawPrecipitationWide(x_origin_prec +                         x_pixel_gap  * i,
                                        y_origin_prec + (FORECAST_FONT.height + y_pixel_gap) * i,
                                        buff, &FORECAST_FONT, BLACK, WHITE);
        }
    }

    #undef FORECAST_FONT
    return ERR_OK;
}


ERROR_T update_temperature(const double inside, const double outside)
{
    #define TEMPERATURE_FONT    font15x9
    const int temp_str_size     = 7;    // big enough to hold temperature format "% 05.1f"
    char buff[temp_str_size];

    ERROR_T ret = ERR_OK;

    const int x_origin_inside   = 35;       //offset
    const int y_origin_inside   = 200;      //191 + offset;
    const int x_origin_outside  = 55;       //EPD_2IN13_V2_WIDTH - offset - temp_str_size * TEMPERATURE_FONT.width;
    const int y_origin_outside  = 225;      //EPD_2IN13_V2_HEIGHT - offset - TEMPERATURE_FONT.height;

    //inside
    Paint_ClearWindows(x_origin_inside,
                       y_origin_inside,
                       x_origin_inside + TEMPERATURE_FONT.width * temp_str_size,
                       y_origin_inside + TEMPERATURE_FONT.height,
                       WHITE);
    if(inside > -100.0){                    // error check if the sensor is not connected
        sprintf(buff, "% 05.1f", inside);       // turn temperature into a string value
        Paint_DrawTemperatureWide(x_origin_inside,
                                  y_origin_inside,
                                  buff, &TEMPERATURE_FONT, BLACK, WHITE);
        Info("--temperature inside: %f\n", inside);
    }else{
        Info("--temperature inside missing.\n");
        ret = ERR_TEMPERATURE_INSIDE;
    }

    //outside
    Paint_ClearWindows(x_origin_outside,
                       y_origin_outside,
                       x_origin_outside + TEMPERATURE_FONT.width * temp_str_size,
                       y_origin_outside + TEMPERATURE_FONT.height,
                       WHITE);
    if(outside > -100.0){                   // error check if the sensor is not connected
        sprintf(buff, "% 05.1f", outside);      // turn temperature into a string value
        Paint_DrawTemperatureWide(x_origin_outside,
                                  y_origin_outside,
                                  buff, &TEMPERATURE_FONT, BLACK, WHITE);
        Info("--temperature outside: %f\n", outside);
    }else{
        Info("--temperature outside missing.\n");
        if(ret == ERR_TEMPERATURE_INSIDE){
            ret = ERR_TEMPERATURE_BOTH;
        }else{
            ret = ERR_TEMPERATURE_OUTSIDE;
        }
    }

    #undef TEMPERATURE_FONT
    return ret;
}


double get_w1_temperature(const char* path)
{
    double tmp_double;  // return value
    FILE* file;

    file = fopen(path, "r");
    if(file != NULL){
        if(fscanf(file, "%lf", &tmp_double) > 0){   // is not empty file
            fclose(file);
            return tmp_double / 1000.0;     // values are stored as string from int of milli degrees of C
        }
        fclose(file);
    }
    return TEMPERATURE_W1_ERROR;
}

int main(void)
{
    weather_struct forecast_data;                       // forecast from internet

    srand(time(NULL));                                  // random generator initiation
    int random_seconds = 0;                             // forecast randomisation update so it is not always "on spot"
    int repeat_counter = 0;                             // if forecast update fails try after a few minutes

    // initiate current time
    PAINT_TIME time;
    UPDATE_T res_update = UPDATE_FULL;                  // start with full display update
    get_time(&time);
    time.sec = UPDATE_SEC_MARK;                         // setting an update flag for the first inf. loop run

    // 1-wire sensors
    double temperature_inside = TEMPERATURE_W1_ERROR;
    double temperature_outside = TEMPERATURE_W1_ERROR;

    // enable SPI interface to the display
    Info("-start\n");
    if(DEV_Module_Init() != 0){
        return ERR_DEV_INIT;
    }
    Info("-module init\n");

    // prepare display
    Info("-epd init\n");
    EPD_2IN13_V2_Init(EPD_2IN13_V2_FULL);
    EPD_2IN13_V2_Clear();

    // build image cache
    UBYTE* img;
    UWORD img_size = ((EPD_2IN13_V2_WIDTH % 8 == 0) ? (EPD_2IN13_V2_WIDTH / 8) : (EPD_2IN13_V2_WIDTH / 8 +1)) * EPD_2IN13_V2_HEIGHT;

    if((img = (UBYTE*)malloc(img_size)) == NULL){
        Info("-failed to allocate image memory\n");
        return ERR_MALLOC;
    }

    // correct orientation of the dissplay
    Info("-correct image orientation\n");
    Paint_NewImage(img, EPD_2IN13_V2_WIDTH, EPD_2IN13_V2_HEIGHT, 180, WHITE);   // vertical orientation
    Paint_SelectImage(img);
    Paint_SetMirroring(MIRROR_HORIZONTAL);
    Paint_Clear(WHITE);

    // signal handling
    signal_flag = 0;    // no signal
    signal(SIGINT, sig_int);
    signal(SIGTERM, sig_term);

    Info("-start main loop\n");

    while(signal_flag == 0){    // infinite loop with main procedure - exit only through signal calls
        // update data in memory before the display is updated
        if(time.sec == UPDATE_SEC_MARK){
            Info("--updating memory at %02i:%02i:%02i %02i.%02i.\n", time.hour, time.min, time.sec, time.day, time.month);

            // update weather forecast if it is outdated
            Info("---check expiration of forecast data.\n");
            if(is_expired(WEATHER_TMP_FILE, - (random_seconds + repeat_counter * UPDATE_DELAY_SEC_NEXT_TRY)) != ERR_EXPIRED_FALSE){     //random offset has to be negative to provide correct offset
                Info("---forecast expired.\n");
                if(download_forecast() == ERR_OK){
                    random_seconds = RANDOM_MIN_DELAY + rand() % RANDOM_MAX_DELAY;  //delay update after expiry
                    repeat_counter = 0;
                    Info("---download succesful.\n");
                    Info("----random_seconds: %i\n", random_seconds);
                }else{
                    repeat_counter++;
                    Info("---download not succesful.\n");
                    Info("----repeat_counter: %i", repeat_counter);
                }
            }

            // read forecast data from the /tmp file
            Info("---forecast read from file.\n");
            forecast_data.valid = get_forecast(&forecast_data);

            // get updated temperature from external sensors
            Info("---update temperature for external sensors.\n");
            temperature_inside  = get_w1_temperature(TEMPERATURE_W1_INSIDE);
            temperature_outside = get_w1_temperature(TEMPERATURE_W1_OUTSIDE);
        }

        // Display updating procedure
        if(res_update != UPDATE_NONE){
            // Update base image after wake-up if there is partial update scheduled
            if(res_update == UPDATE_PART){
                EPD_2IN13_V2_Init(EPD_2IN13_V2_PART);
                EPD_2IN13_V2_DisplayPartBaseImage(img);
            }

            // Image update procedures goes here
            Info("--build image for printing.\n");
            build_gui();            // create static basic gui - it always looks the same

            update_time(&time);
            update_date(&time);
            update_forecast(&forecast_data);
            update_temperature(temperature_inside, temperature_outside);

            // print image on the display
            if(res_update == UPDATE_PART){
                Info("--partial update.\n");
                EPD_2IN13_V2_Init(EPD_2IN13_V2_PART);
                EPD_2IN13_V2_DisplayPart(img);
            }else if(res_update == UPDATE_FULL){
                Info("--full update.\n");
                EPD_2IN13_V2_Init(EPD_2IN13_V2_FULL);
                EPD_2IN13_V2_Display(img);
            }

            // turn sleep mode on the display
            EPD_2IN13_V2_Sleep();
        }

        // delay until next loop iteration
        sleep(1);

        // update time for the next loop (has to be after sleep wake-up)
        // it help to have it at the end so begining of the loop can be properly initialized at first run
        res_update = get_time(&time);
    }

    if(signal_flag==SIGINT){
        Info("\n-signal received: SIGINT\n");
    }else if(signal_flag==SIGTERM){
        Info("\n-signal received: SIGTERM\n");
    }

    // Display shut-off
    Info("-display shut-off\n");
    EPD_2IN13_V2_Init(EPD_2IN13_V2_FULL);
    EPD_2IN13_V2_Clear();
    EPD_2IN13_V2_Sleep();

    // SPI interface disable
    Info("-spi finished\n");
    DEV_Module_Exit();

    // memory cleanup
    Info("-memory cleanup\n");
    free(img);
    img=NULL;

    Info("-exit\n");
    return 0;
}
