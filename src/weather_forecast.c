
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <unistd.h>

#include "weather_forecast.h"
#include "lib/cJSON.h"
#include "lib/Debug.h"


struct string {
    char* ptr;
    size_t len;
};

// PROTOTYPES -----------------------------------------------------------

int init_string(struct string *s);
size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s);
ERROR_T parse_forecast(const char* message, weather_struct* forecast_data);

ERROR_T get_json(const char* path, char** buff);

ERROR_T file_store(const char* path, const char* str);
int get_month_number(const char* month);
double time_difference_gmt(struct tm* time_ex, int delay_sec);
void parse_time_gmt(const char* timestamp, struct tm* t);

// FUNCTIONS ------------------------------------------------------------

ERROR_T get_forecast(weather_struct* forecast)
{
    ERROR_T ret;
    char* buff = (char*)malloc(1 * sizeof(char));

    ret = get_json(WEATHER_TMP_FILE, &buff);
    if(ret == ERR_OK){
        ret = parse_forecast(buff, forecast);
    }
    free(buff);

    return ret;
}


ERROR_T get_json(const char* path, char** buff)
{
    FILE* fp = NULL;
    long file_size = 0;
    ERROR_T ret;

    if(access(path, F_OK) != 0){
        return ERR_FILE_DOESNT_EXIST;
    }

    fp = fopen(path, "r");
    if(fp != NULL){
        fseek(fp, 0L, SEEK_END);
        file_size = ftell(fp);
        rewind(fp);

        *buff = (char*)realloc(*buff, file_size * sizeof(char));    //not the best approach but the file is not that big so it will work fine
        if(*buff != NULL){
            while(!feof(fp)){
                if(fgets(*buff, file_size, fp) != NULL){
                    if(!strncmp(*buff, "{", 1)){     // it't not very solid solution but it should get json data to buffer
                        break;
                    }
                }
            }
        }else{
            free(*buff);
            ret = ERR_GET_FORECAST_BUFFER_REALLOC;
        }
        fclose(fp);

        ret = ERR_OK;
    }else{
        ret = ERR_GET_FORECAST_FILE_OPEN;
    }

    return ret;
}


ERROR_T download_forecast(void)
{
    ERROR_T res = ERR_OK;   //return code of this function

    CURL* curl;
    char* site = "https://api.met.no/weatherapi/locationforecast/2.0/compact?lat="
                  WEATHER_LATITUDE
                  "&lon="
                  WEATHER_LONGITUDE;

    Info("%s\n", site);

    char* user_agents = WEATHER_SOFTWARE_NAME " "
                        WEATHER_SOFTWARE_SITE " "
                        WEATHER_USER_EMAIL;

    CURLcode cc;
    long site_response_code = 0;

    struct string s;
    if(init_string(&s)){
        return ERR_MALLOC;      // problem with malloc() - exit before curl does anything
    }

    curl = curl_easy_init();
    if(curl){
        // setup curl parameters
        curl_easy_setopt(curl, CURLOPT_URL, site);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
        curl_easy_setopt(curl, CURLOPT_HEADER, 1);  //get header as well
        curl_easy_setopt(curl, CURLOPT_USERAGENT, user_agents); // must be included - see yr.no Term of Service

        // perform the curl operation to get data
        cc = curl_easy_perform(curl);

        // check for errors with curl operation
        if(cc != CURLE_OK){
            Error("curl_easy_perform() failed - %s\n", curl_easy_strerror(cc));
            res = ERR_CURL_EASY_PERFORM;
        }else{
            // check site response
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &site_response_code);
            if(site_response_code != HTTP_OK){
                Error("error - HTTP response code: %ld\n", site_response_code);
                res = ERR_CURL_HTTP_WRONG_RESPONSE;
            }
        }

        if(cc == CURLE_OK && res == ERR_OK){     // there was no error so far
            res = file_store(WEATHER_TMP_FILE, s.ptr);
        }
        // always cleanup easy curl
        curl_easy_cleanup(curl);
    }

    free(s.ptr);    // buffer cleanup
    return res;     //current code doesn't care about return value here but it is used for possible further use
}


int init_string(struct string* s) {
    s->len = 0;
    s->ptr = malloc(s->len + 1);
    if (s->ptr == NULL) {
        Error("curl init_string malloc() failed\n");
        return -1;
    }
    s->ptr[0] = '\0';
    return 0;
}


size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
    size_t new_len = s->len + size * nmemb;
    s->ptr = realloc(s->ptr, new_len + 1);
    if(s->ptr == NULL){
        Error("curl writefunc realloc() failed\n");
        return size * (nmemb - 1);      // return different size then it received to mark it with error
    }
    memcpy(s->ptr + s->len, ptr, size * nmemb);
    s->ptr[new_len] = '\0';
    s->len = new_len;
    return size * nmemb;
}


ERROR_T parse_forecast(const char* message, weather_struct* forecast_data)
{
    ERROR_T status = ERR_OK;

    const cJSON* properties = NULL;
    const cJSON* timeseries = NULL;
    const cJSON* timeserie = NULL;
    const cJSON* time = NULL;
    const cJSON* data = NULL;
    const cJSON* instant = NULL;
    const cJSON* next_1_hour = NULL;
    const cJSON* details_instant = NULL;
    const cJSON* details_1_hour = NULL;
    const cJSON* air_temperature = NULL;
    const cJSON* precipitation = NULL;

    int iii = 0;                // timeseries iterator
    char timestamp[30] = {0};   // timeseries timestamp stored as string
    struct tm ts_gmt = {0};

    #ifdef INFO
    int index_counter = 0;      // used only for Info printing
    #endif

    cJSON* buffer_json = cJSON_Parse(message);

    if(buffer_json != NULL){
        properties = cJSON_GetObjectItemCaseSensitive(buffer_json, "properties");
        if(properties != NULL){
            timeseries = cJSON_GetObjectItemCaseSensitive(properties, "timeseries");

            if(timeseries != NULL && cJSON_GetArraySize(timeseries) >= FORECAST_SIZE){  // NULL has to be evauated first
                cJSON_ArrayForEach(timeserie, timeseries){
                    if(timeserie != NULL){time = cJSON_GetObjectItemCaseSensitive(timeserie, "time");}else{status = ERR_CJSON_KEY_TIMESERIE; break;};
                    if(time != NULL){
                        // check if current timeserie is valid for next hour
                        strcpy(timestamp, time->valuestring);
                        parse_time_gmt(timestamp, &ts_gmt);

                        Info("----\n----cJSON item: %i, iii: %i, %s, %02d-%02d-%02dT%02d:%02d:%02d\n",
                             index_counter++, iii, timestamp,
                             ts_gmt.tm_year, ts_gmt.tm_mon, ts_gmt.tm_mday,
                             ts_gmt.tm_hour, ts_gmt.tm_min, ts_gmt.tm_sec);

                        if((time_difference_gmt(&ts_gmt, iii  * 3600) <= 0) && (time_difference_gmt(&ts_gmt, (iii + 1) * 3600) >= 0)){  //one hour time window
                            // data part of the timeserie
                            if(timeserie        != NULL){data               = cJSON_GetObjectItemCaseSensitive(timeserie, "data");                      }else{status = ERR_CJSON_KEY_TIMESERIE;                     break;};
                            // temperature
                            if(data             != NULL){instant            = cJSON_GetObjectItemCaseSensitive(data, "instant");                        }else{status = ERR_CJSON_KEY_DATA_TEMPERATURE;              break;};
                            if(instant          != NULL){details_instant    = cJSON_GetObjectItemCaseSensitive(instant, "details");                     }else{status = ERR_CJSON_KEY_INSTANT_TEMPERATURE;           break;};
                            if(details_instant  != NULL){air_temperature    = cJSON_GetObjectItemCaseSensitive(details_instant, "air_temperature");     }else{status = ERR_CJSON_KEY_DETAILS_INSTANT_TEMPERATURE;   break;};
                            if(air_temperature  != NULL){forecast_data->temperature[iii] = air_temperature->valuedouble;                                }else{status = ERR_CJSON_KEY_AIR_TEMPERATURE;               break;};

                            // precipitation
                            if(data             != NULL){next_1_hour        = cJSON_GetObjectItemCaseSensitive(data, "next_1_hours");                   }else{status = ERR_CJSON_KEY_DATA_PRECIPITATION;            break;};
                            if(next_1_hour      != NULL){details_1_hour     = cJSON_GetObjectItemCaseSensitive(next_1_hour, "details");                 }else{status = ERR_CJSON_KEY_NEXT1HOUR_PRECIPITATION;       break;};
                            if(details_1_hour   != NULL){precipitation      = cJSON_GetObjectItemCaseSensitive(details_1_hour, "precipitation_amount"); }else{status = ERR_CJSON_KEY_DETAILS1HOUR_PRECIPITATION;    break;};
                            if(precipitation    != NULL){forecast_data->precipitation[iii] = precipitation->valuedouble;                                }else{status = ERR_CJSON_KEY_PRECIPITATION;                 break;};

                            Info("-----temperature: %.1f°C,\tprecipitation: %.1fmm\n", forecast_data->temperature[iii], forecast_data->precipitation[iii]);
                            if(++iii == FORECAST_SIZE){break;};
                        }
                    }else{
                        status = ERR_CJSON_KEY_TIME;
                        break;
                    }
                }
                if(iii < FORECAST_SIZE){status = ERR_CJSON_TIMESERIES_OUTDATED;};
            }else{status = ERR_CJSON_KEY_TIMESERIES;}
        }else{status = ERR_CJSON_KEY_PROPERTIES;}
    }else{
        Error("JSON buffer is empty.\n");
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL){
            Error("error before: %s\n", error_ptr);
        }
        status = ERR_CJSON_PARSE_FAILED;
    }

    cJSON_Delete(buffer_json);
    return status;
}


ERROR_T file_store(const char* path, const char* str)
{
    FILE *fp = fopen(path, "wb");
    if(fp != NULL){
        fputs(str, fp);
        fclose(fp);
        return ERR_OK;
    }
    return ERR_WRITE_FILE_OPEN;
}


ERROR_T is_expired(const char* path, int random_delay)
{
    // get expiration time from the file, compare to "now"
    int ret = ERR_EXPIRED_FILE_OPEN;

    FILE* fp = NULL;
    long file_size = 0;
    char* buff = NULL;

    struct tm time_ex = {0};    //expiration time from the file
    int year = 0;       //raw year from year 0 not from 1900
    char mon[10] = {0}; //month as string

    if(access(path, F_OK) != 0){
        return ERR_FILE_DOESNT_EXIST;
    }

    fp = fopen(path, "r");
    if(fp != NULL){

        fseek(fp, 0L, SEEK_END);
        file_size = ftell(fp);
        rewind(fp);

        buff = (char*)malloc(file_size);    //not the best approach but the file is not that big so it will work fine
        if(buff != NULL){
            while(!feof(fp)){
                if(fgets(buff, file_size, fp) != NULL){
                    if(!strncmp(buff, "Expires:", 8)){
                        break;
                    }
                }
            }

            //parse infromation from the file to a tm structure
            sscanf(buff, "%*s %*s %d %s %d %d:%d:%d", &time_ex.tm_mday, mon, &year, &(time_ex.tm_hour), &(time_ex.tm_min), &(time_ex.tm_sec));
            time_ex.tm_year = year - 1900;
            time_ex.tm_mon = get_month_number(mon);

            //comparison
            if(time_difference_gmt(&time_ex, random_delay) > 0.0){
                ret = ERR_EXPIRED_TRUE;
            }else{
                ret = ERR_EXPIRED_FALSE;
            }

            //cleanup
            free(buff);
        }else{
            ret = ERR_EXPIRED_BUFFER_MALLOC;
        }
        fclose(fp);
    }else{
        ret = ERR_EXPIRED_FILE_OPEN;
    }

    return ret;
}


int get_month_number(const char* month)
{
    const char* months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    for(int i=0; i<12 ;i++){
        if(!strncmp(month, months[i], 3)){
            return i;
        }
    }
    return 0;       // it should never reach this point
}


double time_difference_gmt(struct tm* time_ex, int delay_sec)
{
    //compare (time + delay_sec) to "now"
    //time_ex has to be in GMT format
    time_t expiration_time;
    time_t now, now_gmt;
    struct tm* tm_now;

    expiration_time = mktime(time_ex);  //implements difference error between local time and gmt - "now" timer has to have the same error for comparison

    time(&now);
    tm_now = gmtime(&now);
    tm_now->tm_sec += delay_sec;
    now_gmt = mktime(tm_now);           //implements difference error between local time and gmt - expire timer has to have the same error for comparison

    Info("----gmt vs. now: %04d-%02d-%02dT%02d:%02d:%02d ... %04d-%02d-%02dT%02d:%02d:%02d\n",
        time_ex->tm_year, time_ex->tm_mon, time_ex->tm_mday,
        time_ex->tm_hour, time_ex->tm_min, time_ex->tm_sec,
        tm_now->tm_year, tm_now->tm_mon, tm_now->tm_mday,
        tm_now->tm_hour, tm_now->tm_min, tm_now->tm_sec
        );

    return difftime(now_gmt, expiration_time);
}


void parse_time_gmt(const char* timestamp, struct tm* t)
{
    int year;
    int mon;
    sscanf(timestamp, "%d-%d-%d%*c%d:%d:%d", &year, &mon, &(t->tm_mday), &(t->tm_hour), &(t->tm_min), &(t->tm_sec));
    t->tm_year = year - 1900;
    t->tm_mon = mon - 1;
}
