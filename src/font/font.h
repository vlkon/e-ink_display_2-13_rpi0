
#ifndef __FONT_H
#define __FONT_H

#define MAX_HEIGHT_FONT         41
#define MAX_WIDTH_FONT          32
#define OFFSET_BITMAP

#ifdef __cplusplus
    extern "C" {
#endif

#include <stdint.h>

typedef enum {
    NUM_0 = 0,
    NUM_1,
    NUM_2,
    NUM_3,
    NUM_4,
    NUM_5,
    NUM_6,
    NUM_7,
    NUM_8,
    NUM_9,
    NUM_SPACE,
    NUM_C,
    NUM_DEGREE,
    NUM_DOT,
    NUM_COLON,
    NUM_MINUS,
    NUM_PLUS,
    NUM_SLASH
} font_num;


typedef struct
{
  const uint8_t *table;
  uint16_t width;
  uint16_t height;
} font_t;


extern font_t font34x16;
extern font_t font30x16;
extern font_t font15x9;

#ifdef __cplusplus
    }
#endif

#endif /* __FONT_H */
