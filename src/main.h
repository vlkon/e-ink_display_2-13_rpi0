#ifndef __MAIN_H_
#define __MAIN_H_

#define REFRESH_RATE    5   // minutes for full refresh (int 1-60)

#include <stdlib.h>
#include <time.h>

#include "error.h"
#include "lib/EPD_2in13_V2.h"
#include "lib/GUI_Paint.h"
#include "lib/Debug.h"
#include "font/font.h"

#include "weather_forecast.h"

// decision values - how should be the display visualy updated for a current cycle
typedef enum {
    UPDATE_NONE=0,
    UPDATE_FULL,
    UPDATE_PART
}UPDATE_T;

// frames, lines, circles and everything that is a static part of the picture
void build_gui();

// retrieve current system time - used for update cycle evaluation among others
UPDATE_T get_time(PAINT_TIME* pTime);

// update functions that send values to the display buffer
// they are called before each display (visible) update
void update_time(const PAINT_TIME* time);
void update_date(const PAINT_TIME* time);
ERROR_T update_forecast(const weather_struct* forecast);
ERROR_T update_temperature(const double inside, const double outside);

// read temperature value of a device on 1-wire interface
// path is a location of "temperature" file of that device
// e.g. "/sys/bus/w1/devices/28-00000b55c113/temperature" where 28-00000b55c113 is the device
// tested with DS18B20 temperature sensors.
double get_w1_temperature(const char* path);

// error value returned if the sensor is not connected - or it is pretty chilly :)
#define TEMPERATURE_W1_ERROR    -273.15

// currently connected temperature sensors on 1-wire interface
#define TEMPERATURE_W1_INSIDE   "/sys/bus/w1/devices/28-00000b54f82c/temperature"
#define TEMPERATURE_W1_OUTSIDE  "/sys/bus/w1/devices/28-00000b55c113/temperature"

// update memory at certain second - prefered at 55 sec because display updates at 0
#define UPDATE_SEC_MARK             55
#define UPDATE_DELAY_SEC_NEXT_TRY   60

#define RANDOM_MAX_DELAY            900
#define RANDOM_MIN_DELAY            180

#endif //__MAIN_H_