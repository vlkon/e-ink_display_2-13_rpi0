TARGET = epd
SERVICE_NAME = epd

DIR_OBJ = ./obj
DIR_SRC = ./src
DIR_FONT= ./src/font
DIR_LIB = ./src/lib

DIR_INSTALL = /opt/epd

OBJ_C = $(wildcard ${DIR_SRC}/*.c ${DIR_LIB}/*.c ${DIR_FONT}/*.c)
OBJ_O = $(patsubst %.c,${DIR_OBJ}/%.o,$(notdir ${OBJ_C}))

USELIB_RPI = USE_BCM2835_LIB
LIB_RPI = -lbcm2835 -lm -lcurl

CC = gcc
CFLAGS = -O2 -Wall
# device flag
CFLAGS += -D $(USELIB_RPI) -D RPI
# debug flags
#CFLAGS += -g
#CFLAGS += -D DEBUG
#CFLAGS += -D INFO
CFLAGS += -D ERROR

MKDIR = mkdir -p

.PHONY : ${TARGET} clean directories install uninstall

all: directories ${TARGET}

${TARGET}:${OBJ_O}
	$(CC) $(CFLAGS) $(OBJ_O) -o $(TARGET) $(LIB_RPI)

${DIR_OBJ}/%.o:$(DIR_SRC)/%.c
#	$(CC) $(CFLAGS) -c  $< -o $@ -I $(DIR_LIB)
	$(CC) $(CFLAGS) -c  $< -o $@

${DIR_OBJ}/%.o:$(DIR_LIB)/%.c
	$(CC) $(CFLAGS) -c  $< -o $@

${DIR_OBJ}/%.o:$(DIR_FONT)/%.c
	$(CC) $(CFLAGS) -c  $< -o $@

directories: ${DIR_OBJ}

${DIR_OBJ}:
	${MKDIR} ${DIR_OBJ}

clean:
	rm -rf $(DIR_OBJ)
	rm -f $(TARGET)

install:
	${MKDIR} $(DIR_INSTALL)
	cp -rf $(TARGET) $(DIR_INSTALL)/$(TARGET)
	cp -rf $(SERVICE_NAME).service /etc/systemd/system/$(SERVICE_NAME).service
	systemctl enable $(SERVICE_NAME).service
	systemctl start $(SERVICE_NAME).service

install_restart_timer:
	cp -rf $(SERVICE_NAME)_restart.timer /etc/systemd/system/$(SERVICE_NAME)_restart.timer
	cp -rf $(SERVICE_NAME)_restart.service /etc/systemd/system/$(SERVICE_NAME)_restart.service
	systemctl enable $(SERVICE_NAME)_restart.timer
	systemctl start $(SERVICE_NAME)_restart.timer

uninstall:
	systemctl stop $(SERVICE_NAME).service
	systemctl disable $(SERVICE_NAME).service
	rm -rf /etc/systemd/system/$(SERVICE_NAME).service
	rm -rf $(DIR_INSTALL)

uninstall_restart_timer:
	systemctl stop $(SERVICE_NAME)_restart.timer
	systemctl disable $(SERVICE_NAME)_restart.timer
	rm -rf /etc/systemd/system/$(SERVICE_NAME)_restart.timer
	rm -rf /etc/systemd/system/$(SERVICE_NAME)_restart.service
